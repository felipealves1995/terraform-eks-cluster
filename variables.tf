variable "project_name" {
  type = string
}

variable "region" {
  type = string
}

variable "node-group-max-size" {
  type = number
}

variable "instance_types" {
  type = list(string)
}