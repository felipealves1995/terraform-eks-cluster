# Terraform: EKS Cluster CI/CD pipeline

This repository is used to deploy an EKS cluster on AWS using Terraform code and a Gitab CI/CD pipeline.

## Terraform code

The Terraform code is not on this repository.
I wrote it as a [module](https://github.com/luizscofield/terraform-eks) and published on Github.
To use the module, you only need to configure it as a source:

```
module "eks-cluster" {
  source              = "github.com/luizscofield/terraform-eks?ref=v1.1"
  project_name        = var.project_name
  global_tags         = local.tags
}
```

[Check out the documentation on Github to learn more.](https://github.com/luizscofield/terraform-eks)

## The CI/CD pipeline

This pipeline runs on every commit on the repository.
The **"validate"** and **"plan"** stages will **always** run. But the **"apply"** stage only runs on the branch **main**.

No one can **push** to branch **main**. The only way to commit to **main** is by creating a **merge request** from another branch.
When a new branch is created, the **Terraform Plan** will be **cached**.
It will be displayed on the **merge request** to be reviewed.

If the **merge request** is **approved**, the cached plan will be **applied**.