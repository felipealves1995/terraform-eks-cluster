locals {
  tags = {
    Project     = var.project_name
    Environment = "Development"
    Team        = "DevOps"
    Nova_tag    = "Testando CI/CD"
  }
}